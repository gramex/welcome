<br/>
<hr/>
# **GramEx** : Votre assistant personnel pour générer des parcours d'apprentissage de la grammaire
<hr/>

<br/>
## Générer vos exercices en quelques minutes
<br/>
**GramEx** est à la fois un générateur d'exercices de grammaire et un exerciseur (outil permettant à l'apprenant&middot;e de répondre directement aux exercices et de recevoir un retour en temps réel sur ses réussites et échecs).
**GramEx**  est destiné aux enseignant&middot;e&middot;s et apprenant&middot;e&middot;s (élèves, étudiant&middot;e&middot;s, professionnel&middot;le&middot;s).

![](img/undraw_annotation_re_h774.svg){: style="height:250px;width:250px" align=right }

<br/>
## Suivez l'évolution de vos publics
<br/>
**GramEx** permet de suivre les performances de vos publics (élèves, groupes d'élèves, classes) dans le temps, ses outils de reporting vous aident à mettre en place une différenciation pédagogique.

<br/>
## Bénéficiez des résultats du monde de la recherche

**GramEx** utilise des techniques issues de la recherche en Traitement Automatique des Langues (TAL) pour automatiser la création d'exercices de grammaire à partir de textes sélectionnés par l'enseignant&middot;e.

<br/>
## Nos partenaires

**GramEx** est soutenu par :

* le [Laboratoire lorrain de recherche en informatique et ses application (Loria)](https://www.loria.fr)
* l'[Université de Lorraine](https://www.univ-lorraine.fr)
* le [Centre National de la Recherche Scientifique (CNRS)](https://www.cnrs.fr) / [CNRS Innovation](https://www.cnrsinnovation.com/a-propos/)
* la [Délégation Régionale Académique au Numérique Éducatif (DRANE)](https://dane.daneteach.fr/) de l'Académie de Nancy-Metz (via le programme PLANETE) 

|     |     |     |     |     |
|:---:|:---:|:---:|:---:|:---:|
|![LORIA](img/LORIA.png)|![UL](img/ul-logo.png)|![CNRS Innovation](img/logo-cnrs-innovation2.png)|![PLANETE](img/logo_planete.png)|![Logo DRANE](img/logo_drane.png)|
