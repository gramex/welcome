## On parle de **GramEx**
<br/>

![](img/undraw_social_update_re_xhjr.svg){: style="height:250px;width:250px" align=right }

* Article [Un projet du CNRS Centre-Est accepté en prématuration](https://www.centre-est.cnrs.fr/fr/cnrsinfo/un-projet-du-cnrs-centre-est-accepte-en-prematuration), lettre d'information de la délégation Centre-Est du CNRS

* Article [Recherche en e-éducation - Projet GramEx](https://dane.daneteach.fr/gramex/), site de la délégation régionale académique au numérique éducatif (DRANE) du rectorat de l'académie de Nancy-Metz

<br/>
## Travaux de recherche en lien avec **GramEx**
<br/>

* Colin, É., & Gardent, C. (2018). Generating Syntactic Paraphrases. In Proceedings of the 2018 Conference on Empirical Methods in Natural Language Processing (p. 937‑943). Brussels, Belgium: Association for Computational Linguistics. [https://doi.org/10.18653/v1/D18-1113](https://doi.org/10.18653/v1/D18-1113)

* Colin, E., & Gardent, C. (2019). Generating Text from Anonymised Structures. In Proceedings of the 12th International Conference on Natural Language Generation (p. 112‑117). Hong Kong, China. [https://hal.science/hal-02460312](https://hal.science/hal-02460312)

* Colin, É. (2020). Traitement automatique des langues et génération automatique d’exercices de grammaire (Theses No. 2020LORR0059). Université de Lorraine. [https://hal.univ-lorraine.fr/tel-02949349](https://hal.univ-lorraine.fr/tel-02949349)

* Ngo, D. V., & Parmentier, Y. (2023). Towards Sentence-level Text Readability Assessment for French. In Second Workshop on Text Simplification, Accessibility and Readability (TSAR@RANLP2023). Varna, Bulgaria. [https://hal.science/hal-04192063](https://hal.science/hal-04192063)

![](img/undraw_data_processing_yrrv.svg){: style="height:250px;width:250px" align=right }

* Perez-Beltrachini, L., Gardent, C., & Kruszewski, G. (2012). Generating Grammar Exercises. In The 7th Workshop on Innovative Use of NLP for Building Educational Applications, NAACL-HLT Worskhop 2012 (p. 147‑157). Montreal, Canada. [https://hal.science/hal-00768610](https://hal.science/hal-00768610)



