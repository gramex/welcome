# L'équipe
<br/>

## Membres

Le program **GramEx** est portée par deux personnels de recherche et un ingénieur R&D du laboratoire [Loria](https://www.loria.fr) (Unité Mixte de Recherche 7503 CNRS - Université de Lorraine) :

::cards::

- title: Claire Gardent
  content: Directrice de Recherche au CNRS <br/>(09/2022-)
  image: ./img/claire.png
  url: https://members.loria.fr/CGardent/

- title: Yannick Parmentier
  content: Maître de conférences à l'Université de Lorraine <br/>(09/2022-)
  image: ./img/yannick.png
  url: https://sourcesup.renater.fr/www/tulipa/yannick

- title: Guillaume Toussaint
  content: Ingénieur contractuel au CNRS <br/>(05/2023-)
  image: ./img/icons/007-poseidon.png

::/cards::

<br/>
## Contributions

Le projet **GramEx** a accueilli plusieurs stagiaires depuis le lancement du projet :

::cards::

- title: Hugo Collin
  content: Stagiaire de BUT Informatique, IUTNC <br/>(02/2023-05/2023)
  image: ./img/hugo.png
  url: https://hugocollin.com

- title: Lucas Poirot
  content: Stagiaire de BUT Informatique, IUT St Dié <br/>(02/2023-05/2023)
  image: ./img/icons/006-ares.png

- title: Lucas Maujean
  content: Stagiaire de BUT Multimédia et Métiers de l'Internet <br/>(03/2023-06/2023)
  image: ./img/icons/009-hermes.png

::/cards::
