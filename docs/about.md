# À propos de **GramEx**
<br/>

!!! note "Disponible prochainement"
    **GramEx** est en phase de développement et sera accessible prochainement.

## **GramEx**

- permet de **générer des exercices de grammaires à partir de textes sélectionnés** par ses soins, ce qui permet des pratiques transdisciplinaires, ou encore de réinvestir des apprentissages ; par exemple, suite à une activité théâtrale en cours de français sur un texte de Molière, l’enseignant peut utiliser **GramEx** pour créer des exercices de grammaire à partir des phrases de ce même texte) ;

- de **générer un nombre potentiellement illimité d'exercices** (**GramEx** permet non seulement d'analyser tout type de texte, mais de plus il extrait l'ensemble des phénomènes grammaticaux présents dans ces derniers pour générer un maximum d’exercices) ;

- est **configurable**, on peut par exemple associer un ensemble de notions grammaticales à un niveau scolaire donné ;

- permet de **définir des parcours d'apprentissage personnalisés** (sessions d'exercices particulières), ce qui rend possible une différentiation pédagogique contrôlée par l’enseignant et assistée par ordinateur.

![](img/undraw_in_progress_re_m1l6.svg){: style="height:250px;width:250px" align=right }


