# The GramEx website

This repository hosts the source code of the GramEx website. It is written in markdown, and compiled by [Mkdocs](https://mkdocs.org), into a static web site (written in CSS/JS/HTML).

## Structure

Basically a mkdocs-based website is composed of:

- a configuration file (named `mkdocs.yml` and located at the root of the repository) 

- a directory (named `docs` located at the root of the repository) where to store md files (each one corresponding to a page of the website)

The `mkdocs.yml` file specifies:

* which graphical theme and plugins are used,
* the content of the side menu
* the logo and favicon locations

## Requirements

First, install the requirements (mkdocs, dracula theme and neoteroi plugin):

     pip install -r requirements.txt

Note that you can first set up an isolated virtual environment by invoking:

     python3 -m venv venv
     source venv/bin/activate


## Compilation

To compile and visualize the development version of the website, invoke mkdocs as follows:

     mkdocs serve

You can preview the website by going to 127.0.0.1:8000 in your web browser.

To compile the static site:

     mkdocs build

The resulting website will be created under `site/`.

## Deployment

To deploy the website, one has to copy the content of `site/` where the site is hosted (webserver).

We use the gitlabpages to do so. The configuration is stored in `.gitlab-ci.yml`.

Whenever a new version of the sources is pushed to the gitlab remote repo (server), the pages are automatically copied to the web server.

## Contact

[Yannick Parmentier](mailto:yannick.parmentier@loria.fr)
